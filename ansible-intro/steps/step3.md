Tenemos nuestra carpeta base, y en principio, **poco más vamos a necesitar para crear nuestro primer playbook**. 
 
Aún así, **es recomendable crearnos nuestro propio inventario para nuestro playbook de Ansible**: muchas veces tendremos playbooks que no tienen que ver entre ellos y la mejor manera de diferenciarlos es hacer que cada uno pueda única y exclusivamente atacar a las máquinas que sean necesarias.  
 
Para empezar, vamos a meternos dentro del directorio de Ansible (si no lo hemos hecho ya): `cd ansible`{{execute}} 
 
#### ¿Qué es un inventario? 
Especificado bajo el nombre '*inventory*', un inventario viene a ser un **fichero (o una serie de ellos) que especifican los hosts a los que va a atacar nuestro playbook de Ansible**. Como adelanta la frase anterior, no tiene por qué ser uno solo, y cada playbook puede utilizar cualquiera de ellos.  
 
#### Creando nuestro primer inventory 
Los ficheros de inventario suelen ubicarse bajo una carpeta que está en al mismo nivel que el *Playbook*: 'inventory'. Esta carpeta suele contener un fichero llamado "*hosts*", aunque el nombre puede ser el que queramos, sin ningún tipo de problema (solo que de tener un nombre distinto tendremos que referenciarlo al llamar al comando del playbook). 
 
Para ello vamos a utilizar un comando ya conocido: `mkdir inventory`{{execute}} 
 
Y bajo él vamos a crear un inventory simple... 

`echo "[group1]" > inventory/myhosts`{{execute}} 
`echo "host01 ansible_ssh_user=ubuntu" >> inventory/myhosts`{{execute}} 

 
Listo, en nuestro IDE ya deberíamos ver cómo, bajo la carpeta *ansible/inventory*, ya tenemos nuestro fichero hosts rellenado. 
 
#### Ad-hoc task
To run an ad-hoc task, using a single module, use ansible.

For example, to check the current date and time of the remote host:


`ansible group1 -i inventory/myhosts -m command -a date`{{execute}}


(if the command fails the first time, try again in few seconds, as the test host may not yet have been provisioned)

group1 = group to run against. You can also use the name of an individual host, so host01 would work just as well for this example.

myhosts = name of the inventory file listing all available hosts

command = module to use (the command module returns the result of a bash command)

date = argument for the module, in this case a standard shell command date.

When you run the command above, Ansible establishes an SSH connection with the remote host, executes the command, captures the output and returns it.



#### Ansible y el lenguaje YAML 
Ya tenemos una **base clara, donde empezar a ejecutar nuestro Playbook aislado**. Ahora lo único que queda es crear nuestro playbook y rellenarlo. 
 
Para ello, vamos a crear el fichero tal cual: 

`touch site.yml`{{execute}} 
 
Como podemos ver, **su extensión es "*.yml*"**. Esto significa que es un fichero del tipo "*yaml*", formato que se utiliza para serializar datos de manera legible y que está **inspirado en lenguajes como *C*, *Python*, *XML* o *Perl***.  
 
Es **bastante utilizado**, no solo por Ansible, sino que también por frameworks como *Laravel* o la solución **PaaS** de *Red Hat: Openshift*.  
 

#### ¿Qué es un Playbook?

**Ansible funciona a través del uso de unos ficheros donde se le dice lo que hay que ejecutar**. Podemos pensar en un *playbook* como una lista con todas las tareas que hay que realizar para conseguir lo que queremos. 

Los *playbooks* **tienen varias formas distintas de utilizarse**: existen configuraciones donde se listan las tareas tal cual, así como *playbooks* que llaman a un *rol*, que es el que tiene la lista de tareas, junto a otros ficheros; **también existen *playbooks* que llaman a otros ficheros *.yml* donde se encuentran las tareas** y si vamos más allá, ***playbooks* que llaman a otros *.yml* que llaman a su vez a un rol dentro de ellos, cuyo fichero *.yml* interior llama a otros ficheros *.yml* donde se encuentran las tareas...**

 
#### Rellenando nuestro playbook.yml 
 
Vamos a rellenar, entonces, nuestro ***site.yml***. Esta es la configuración recomendada: 
 
<pre class="file" data-filename="site.yml" data-target="replace">--- 
- hosts: host01

  become: true

  tasks:
    - name: ensure latest sysstat is installed
      apt:
        name: sysstat
        state: latest
</pre>

`ansible-playbook -i inventory/myhosts site.yml`{{execute}} 

Ansible should return the result 'Changed=1', indicating that the package was installed.

#### Playbook Explicación
What happened here?

--- denotes the beginning of a YAML file
hosts: host tells Ansible to run the tasks on the host host
become: true makes all your tasks run as sudo
name: is basically a comment, describing what the task does
***apt***: specifies the module we want to use
name: is an argument to the apt module, that specifies the name of the package to install.
To see all arguments for a specific module, allowed values, and other details, you can use the CLI documentation that is included with Ansible:

**ansible-doc apt***

To close the documentation, enter q in the terminal.