# ¿Qué es Ansible?
Ansible es una **plataforma de software libre que permite configurar y administrar equipos informáticos**, o así es como se describe en la Wikipedia.
En la práctica, Ansible no sirve única y exclusivamente para eso, sino que también nos permite generar imágenes Docker, subir/descargar artefactos de un repositorio y generar ficheros que variarán según la ejecución gracias a la posibilidad de parametrizar plantillas. 

**Para un desarrollador esto es muy interesante**, y es muy común ver Ansible como motor para lograr la Integración Continua y la Entrega Continua en equipos de desarrollo de Software.

A través de este pequeño curso, se pretende enseñar a quien le interese a utilizar Ansible, de manera totalmente interactiva y guiada, además de proponer ejercicios para realizar en un entorno propio con más de una máquina e incluso de manera remota.

## ¿Qué aprenderemos en este pequeño escenario?
La idea es **tener clara cuál es la base del árbol de directorios típico para un proyecto de Ansible**. Saber qué contiene cada carpeta y qué uso tiene dentro de la jerarquía, para así ser capaces de encontrar y solventar errores de manera más ágil, sin perder mucho tiempo en ello.

**Crearemos nuestros propios roles y entenderemos cómo funcionan**. 

Además, podremos reutilizar nuestro entorno para extender la práctica, a través de unas ideas de ejercicios para que practiques.

**¡comenzar!**
