¡Bienvenido al Playground de Ansible!

Aquí podrás practicar a gusto, sin pasos, sin entorno predefinido ni acotado.

Si quieres aprender con entornos controlados, puedes probar estos:
- [Ansible - Intro](https://katacoda.com/fmoreno/scenarios/ansible-intro)
- [Ansible - Roles](https://katacoda.com/fmoreno/scenarios/ansible-roles)

