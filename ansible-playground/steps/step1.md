Por favor, **espera mientras se inicializa el Playground** (instalación de Ansible sobre CentOS).

*Una vez veas que se crea el directorio "ansible" en tu ventana del IDE, ya podrás empezar a jugar con Ansible.*

Documentación de Ansible: [aquí](http://docs.ansible.com/).
